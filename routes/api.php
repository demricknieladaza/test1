<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Add Contact
Route::post('addContact', 'ContactsController@store');

// Get Contacts
Route::get('getContacts/{id}/{email}', 'ContactsController@myContacts');

Route::get('getRequests/{id}/{email}', 'ContactsController@getRequests');

Route::put('UpdateRequest/{id}/', 'ContactsController@UpdateRequest');
