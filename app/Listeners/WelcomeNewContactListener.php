<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterMail;

class WelcomeNewContactListener implements ShouldQueue
{
   
    public function handle($event)
    {
        sleep(10);

        Mail::to($event->email)->send(new RegisterMail());
    }
}
