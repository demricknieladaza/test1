<?php

namespace App\Http\Controllers;

use App\Contact;
use App\User;
use App\Mail\RegisterMail;
use App\Events\NewContactAddedEvent;
use App\Events\AcceptMailEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use Auth;

class ContactsController extends Controller
{
    protected $user;

    public function __construct()
    {
        
    }

    public function myContacts($id,$email){
        $contacts = Contact::where('status','accepted')
                    ->where('user_id', $id)
                    
                    ->get();

        return $contacts;
    }

    public function getRequests($id,$email){
        $contacts = Contact::where('email',$email)
                    ->where('status','pending')
                    ->get();
        return $contacts;
    }
    
    public function store(Request $request){
        $contact = new Contact;
        $email = $request->input('email');
        $contact->email = $email;
        $contact->tags = $request->input('tags');
        $contact->notes = $request->input('notes');
        $contact->user_id = $request->input('user_id');
        $contact->status = 'pending';

        $contact->save();

        $id = User::where('email',$email)
                    ->get();
        
        if($id){
            event(new AcceptMailEvent($email));
        }
        else{
            event(new NewContactAddedEvent($email));
        }

        return 1;
    }

    public function UpdateRequest($id){
        $contact = Contact::find($id);

        $contact->status = 'accepted';

        $contact->save();

        return 1;
                   
    }
}