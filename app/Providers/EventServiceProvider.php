<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Events\NewContactAddedEvent;
use App\Events\AcceptMailEvent;
use App\Listeners\WelcomeNewContactListener;
use App\Listeners\AcceptMailListener;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        NewContactAddedEvent::class => [
            WelcomeNewContactListener::class,
        ],
        AcceptMailEvent::class => [
            AcceptMailListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
