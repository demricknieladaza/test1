@component('mail::message')
# Hello!

You have been invited!

@component('mail::button', ['url' => 'http://127.0.0.1:8000/register'])
Register
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
