@component('mail::message')
# Hi 
Someone wants to add you as trusted contact.
@component('mail::button', ['url' => 'http://127.0.0.1:8000/requests'])
Accept
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
